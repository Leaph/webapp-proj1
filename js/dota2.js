window.onload = function () {

document.getElementById("myForm").onclick = function() {
  getPlayerLastMatchId(document.getElementById('dotaId').value);
}
document.getElementById("myForm2").onclick = function() {
  getPlayerLastMatchId(document.getElementById('dotaId2').value);
}
document.getElementById("myForm3").onclick = function() {
  getPlayerLastMatchId(document.getElementById('dotaId3').value);
}
document.getElementById("myForm4").onclick = function() {
  getRandomHero();
}
var heroes = [];
var heroWithAttr = [];

function getWinStatus(match, id) {
  for (var i = 0; i < match.players.length; i++) {
    var player = match.players[i];
    if (player.account_id == id && player.player_slot < 5 && match.radiant_win == true) {
      return true;
    }
    else if (player.account_id == id && player.player_slot > 5 && match.radiant_win == false) {
      return true;
    }
  }
  return false;
}

function getWinningPicks(match) {
  heroes = [];
  for (var i = 0; i < match.players.length; i++) {
    var player = match.players[i];
    if (player.player_slot < 5 && match.radiant_win == true) {
      heroes.push(player.hero_id)
    }
    else if (player.player_slot > 5 && match.radiant_win == false) {
      heroes.push(player.hero_id)
    }
  }
  return heroes;
}

function getPlayerStats(match, id) {
  for (var i = 0; i < match.players.length; i++) {
    var player = match.players[i];
    if (player.account_id == id) {
      return player;
    }
  }
}

function getMatchHeroAttributeDist(match) {
  var str = 0;
  var agi = 0;
  var intel = 0;
  var melee = 0;
  var range = 0;
  for (var i = 0; i < match.players.length; i++) {
    var player = match.players[i];
    var heroId = player.hero_id-1;
    if (heroWithAttr[heroId].attack == "ranged") {
      range++;
    } else {
      melee++;
    }

    if (heroWithAttr[heroId].primaryAttribute == "intellect") {
      intel++;
    } else if (heroWithAttr[heroId].primaryAttribute == "strength") {
      str++;
    } else {
      agi++;
    }
  }
  return [{"str":str, "agi":agi, "intel":intel },{"melee":melee, "range":range}]
}

function getMactchDetailsById(id, playerId) {
  var first = "http://cdn.dota2.com/apps/dota2/images/heroes/"
  var end = "_full.png?v=3840381?v=3840381"
  x = new XMLHttpRequest();
  x.open("GET","http://localhost:8001/getmatchdetails?id="+id, true);
  x.onreadystatechange=function(){
    if (x.readyState==4 && x.status==200){
      res = JSON.parse(x.response);
      console.log(res)
      document.getElementById('game_duration').innerHTML = "Game duration: "+Math.round((res.duration/60)*100)/100;
      document.getElementById('first_blood').innerHTML = "First blood: "+Math.round((res.first_blood_time/60)*100)/100;
      document.getElementById('game_start').innerHTML = "Game started: "+new Date(res.start_time*1000);
      document.getElementById('win_status').innerHTML = "Win: " +getWinStatus(res, playerId)
      var player = getPlayerStats(res, playerId)
      document.getElementById('gpm').innerHTML = "Gold per Minute: " + player.gold_per_min
      document.getElementById('gold_spent').innerHTML = "Total Gold Spent: "+ player.gold_spent
      document.getElementById('hero_damage').innerHTML = "Total Hero Damage: "+player.hero_damage
      document.getElementById('hero_heal').innerHTML = "Total Hero Healing: "+player.hero_healing
      document.getElementById('last_hit').innerHTML = "Last hits: "+player.last_hits
      document.getElementById('level').innerHTML = "Level: " +player.level
      document.getElementById('xpm').innerHTML = "Experience Gained per Minute: "+player.xp_per_min
      document.getElementById('hero').innerHTML = "Hero: "+heroes[player.hero_id-1].localized_name
      document.getElementById('myHero').src = getImageSrc(heroes[player.hero_id-1].localized_name);
      document.getElementById('deny').innerHTML = "Denies: "+player.denies
      document.getElementById("kills").innerHTML = player.kills
      document.getElementById("deaths").innerHTML = player.deaths
      document.getElementById("assists").innerHTML = player.assists
      document.getElementById("tower_damage").innerHTML = player.tower_damage
      console.log(player)
      document.getElementById("win0name").innerHTML = heroWithAttr[getWinningPicks(res)[0]-1].name
      document.getElementById("win0role").innerHTML = Object.keys(heroWithAttr[getWinningPicks(res)[0]-1].roles)
      document.getElementById("win0Image").src = getImageSrc(heroWithAttr[getWinningPicks(res)[0]-1].name);
      document.getElementById("win1name").innerHTML = heroWithAttr[getWinningPicks(res)[1]-1].name
      document.getElementById("win1role").innerHTML = Object.keys(heroWithAttr[getWinningPicks(res)[1]-1].roles)
      document.getElementById("win1Image").src = getImageSrc(heroWithAttr[getWinningPicks(res)[1]-1].name);
      document.getElementById("win2name").innerHTML = heroWithAttr[getWinningPicks(res)[2]-1].name
      document.getElementById("win2role").innerHTML = Object.keys(heroWithAttr[getWinningPicks(res)[2]-1].roles)
      document.getElementById("win2Image").src = getImageSrc(heroWithAttr[getWinningPicks(res)[2]-1].name);
      document.getElementById("win3name").innerHTML = heroWithAttr[getWinningPicks(res)[3]-1].name
      document.getElementById("win3role").innerHTML = Object.keys(heroWithAttr[getWinningPicks(res)[3]-1].roles)
      document.getElementById("win3Image").src = getImageSrc(heroWithAttr[getWinningPicks(res)[3]-1].name);
      document.getElementById("win4name").innerHTML = heroWithAttr[getWinningPicks(res)[4]-1].name
      document.getElementById("win4role").innerHTML = Object.keys(heroWithAttr[getWinningPicks(res)[4]-1].roles)
      document.getElementById("win4Image").src = getImageSrc(heroWithAttr[getWinningPicks(res)[4]-1].name);
      var matchStats = getMatchHeroAttributeDist(res) //0 for str/agi/int, 1 for melee/range
      // document.getElementById("str").aria-valuetransitiongoal = 80
      console.log(matchStats)
    }
  }
  x.send();
}

function getPlayerLastMatchId(id) {
  console.log(id)
  l = new XMLHttpRequest();
  l.open("GET","http://localhost:8001/getmatchhistory?id="+id, true); //64980686
  l.onreadystatechange=function(){
    if (l.readyState==4 && l.status==200){
    res = l.response;
    getMactchDetailsById(l.response, id)
    }
  }
  l.send();
}
// getPlayerLastMatchId(64980686)

function getHeroes() {
  m = new XMLHttpRequest();
  m.open("GET","http://localhost:8001/getheroes", true); //64980686
  m.onreadystatechange=function(){
    if (m.readyState==4 && m.status==200){
    res = JSON.parse(m.response).heroes;
    // console.log(res[65]) //65 being the hero id -1 since it starts from 1: Anti Mage
    heroes = res;
    }
  }
  m.send();
}
getHeroes()

function getHeroesWithAttribute() {
  xhr = new XMLHttpRequest();
  xhr.open("GET","http://localhost:8001/getheroeswithattribute", true); //64980686
  xhr.onreadystatechange=function(){
    if (xhr.readyState==4 && xhr.status==200){
    res = JSON.parse(xhr.response);
    heroWithAttr = res;
    }
  }
  xhr.send();
}
getHeroesWithAttribute()

function getImageSrc(hero) {
   var first = "http://cdn.dota2.com/apps/dota2/images/heroes/"
  var end = "_full.png?v=3840381?v=3840381"
  hero = hero.toLowerCase();
  hero = hero.replace(/\s/g, "_");
  hero = first+hero+end
  return hero;
}

function getRandomHero() {
  var first = "http://cdn.dota2.com/apps/dota2/images/heroes/"
  var end = "_full.png?v=3840381?v=3840381"
  console.log(end)
  xmlhr = new XMLHttpRequest();
  xmlhr.open("GET","http://localhost:8001/getrandomhero", true); //64980686
  xmlhr.onreadystatechange=function(){
    if (xmlhr.readyState==4 && xmlhr.status==200){
    res = xmlhr.response;
    console.log(xmlhr.response)
    document.getElementById("randomname").innerHTML = res
    res = getImageSrc(res)
    document.getElementById("imageId").src=res;
    console.log(first+res+end)
    }
  }
  xmlhr.send();
}
getRandomHero()
function getTotalPlayersOnline() {
  xml = new XMLHttpRequest();
  xml.open("GET","http://localhost:8001/", true);
  xml.onreadystatechange=function(){
    if (xml.readyState==4 && xml.status==200){
      res = xml.response;
      console.log(xml.response)
      res = String(res)
      document.getElementById('p1').innerHTML = res;
      document.getElementById('p2').innerHTML = res;
      document.getElementById('p3').innerHTML = res;
      }
  }
  xml.send();
}
 getTotalPlayersOnline();
}
