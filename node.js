const key = '7C1D66BE1E6F0EB7212CAA56EA9B14A5'

const SteamApi = require('steam-api');
const userStats = new SteamApi.UserStats(key, 64980686);
const gameId = 570
const Dota2Api = require('dota2api');
const dota = new Dota2Api(key);
const dota2Info = require('dota2-info');
const names = require('dota2-heroes');
const allNames = names.all;

// const dota2Info = require('dota2-info');
// const heroes = dota2Info.getHeroes({primaryAttribute:"agility"});
// console.log(heroes)

// dota.getMatchDetails('3006418471', function (err, res) {
//   console.log(res.duration/60) //duration of the game
//   console.log(res.radiant_win) //radiant win or not
// });

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var http = require('http'),
      fs = require('fs'),
     url = require('url')

http.createServer(function(request, response){
  response.writeHead(200, {"Content-Type": "text/plain", 'Access-Control-Allow-Origin': 'http://localhost:8000'});
    var path = url.parse(request.url).pathname;
    console.log(path)
    var steamID = getParameterByName('id', request.url);

    if (path == "/") {
      userStats.GetNumberOfCurrentPlayers(gameId).done(function(result){
        console.log("total players on steam right now:", result);
        response.end(""+result)
      });
    } else if (path == "/getmatchhistory") {
      dota.getByAccountID(steamID, function (err, res) {
        if (res.status == 1) {
          response.end(JSON.stringify(res.matches[0].match_id))
        } else {
          response.end(null)
        }
      });
    } else if (path == "/getmatchdetails") {
      dota.getMatchDetails(steamID, function (err, res) {
        // console.log(res)
        // console.log(res.start_time)
        // console.log(res.radiant_win)
        // console.log(res.duration)
        // console.log(res.first_blood_time)
        string = res.radiant_win;
        response.end(JSON.stringify(res));
      });
    } else if (path == "/getheroes") {
      dota.getHeroes(function (err, res) {
        // console.log(res)
        response.end(JSON.stringify(res))
      }); 
    } else if (path == "/getheroeswithattribute") {
      response.end(JSON.stringify(dota2Info.getHeroes()));
    } else if (path == "/getrandomhero") {
      response.end(names.random())
    } else {
      response.end(null)
    }
}).listen(8001);
console.log("server initialized");